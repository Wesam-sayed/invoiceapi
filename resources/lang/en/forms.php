<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'buttons'=>[
        'choose_option'=>'Choose an Option',
    ],
    'form_header'=>[
        'account'=>'Account',
        'about_user'=>'About User',
        'add_category'=>'Add Category',
        'add_status'=>'Add Status',
        'statuses' => 'Statuses',
        'user_profile'=>'User\'s Profile',
        'user_groups'=>'Groups / Company Data',
        'user_credentials'=>'Privacy User Data',
    ],
    'user'=>[
        'buttons'=>[
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
            'Clients' => 'Clients',
            'client' => 'client',
        ],
        'fname' => 'First Name',
        'lname' => 'Last Name',
        'name' => 'Name',
        'discount' => 'Discount',
        'from' => 'From',
        'to' => 'To',
        'final_status'=> 'Final Status',
        'select_status' => 'Select Status',
        'email' => 'Email',
        'password' => 'Password',
        'confirm_password' => 'Confirm Password',
        'dob' => 'Date of Birth',
        'gender' => 'Select Gender',
        'branch' => 'Select Branch',
        'sales_rep' => 'Sales Representative',
        'sales_rep_yes' => 'Yes',
        'sales_rep_no' => '-',
        'male' => 'Male',
        'female' => 'Female',
        'thumbnail' => 'Image Thumbnail',
        'image_profile' => 'Image Profile',
        'user_group' => 'Select Group',
        'company' => 'Select Company',
        'companyName' => 'Company Name',
        'table_header' => 'Users',

        'table_header_desc' => 'Company\'s Employees',
    ],
    'edit_user'=>[
        'buttons'=>[
            'cancel' => 'Cancel',
            'edit_company' => 'Edit Company:',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
            'profile' => 'Profile',
            'change_password' => 'Change Password',
        ],
        'user_info'=>'User Information:',
        'edit_user'=>'Edit User',
        'fname' => 'First Name',
        'lname' => 'Last Name',
        'email' => 'Email',
        'password' => 'Password',
        'change_password' => 'Change Password',
        'new_password' => 'New Password',
        'new_confirm_password' => 'Confirm New Password',
        'confirm_password' => 'Confirm Password',
        'dob' => 'Date Of Birth',
        'gender' => 'Choose Gender',
        'branch' => 'Select Branch',
        'male' => 'Male',
        'female' => 'Female',
        'thumbnail' => 'Image Thumbnail',
        'image_profile' => 'Image Profile',
        'user_group' => 'Choose Group',
        'company' => 'Company',
        'table_header' => 'Users',
        'table_header_desc' => 'Company Employees',
        'user_active' => 'Active',
    ],
    'company'=>[
        'buttons'=>[
            'add_company' => 'Add Company',
            'edit_company' => 'Edit Company',
            'company_setting' => 'Co. Settings',
            'add_company_brand' => 'Add Co. Brand',
            'add_company_currency' => 'Add Co. Currency',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'CustomerGroup' => 'Customer Group',
            'submit' => 'Submit',
        ],

        'setting' => 'Company Settings',
        'location' => 'Address Setup',
        'completed' => 'Review Form',
        'company_details' => 'Company Details',
        'name' => 'Company Name',
        'email' => 'Email',
        'mobile' => 'Mobile',
        'landline' => 'Landline',
        'website' => 'Website',
        'company_type' => 'Choose Company Type',
        'active' => 'Active',
        'female' => 'Female',
        'logo' => 'Company Logo',
        'address' => 'Address',
        'address_span' => 'Please Insert Address',
        'country' => 'Country',
        'select_country' => 'Select Country',
        'state' => 'State',
        'select_state' => 'Select State',
        'area' => 'Region',
        'select_area' => 'Select Region',
        'city' => 'City',
        'select_city' => 'Select_City',
        'table_header' => 'Companies',
        'table_header_desc' => 'Company Employees',
        'customer_transactions' => 'customer transactions',
        'personal_info' => 'Personal Information ',
    ],
    'group'=>[
        'buttons'=>[
            'add_group' => 'Add Group',
            'edit_group' => 'Edit Group',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],

        'setting' => 'Group Settings',
        'location' => 'Address Settings',
        'completed' => 'Review Submission',
        'group_details' => 'Group Details',
        'group_submit_form' => 'Review Groups Details and Submit',
        'name' => 'Group Name',
        'selected_company' => 'Selected Company',
        'company' => 'Select Company',
        'active' => 'Active',
        'table_header' => 'Groups',
        'table_header_desc' => 'Company\'s Group',
    ],
    'permission'=>[
        'buttons'=>[
            'add_group' => 'Add Permissions',
            'edit_group' => 'Edit Permissions',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
            'save' => 'Save',

        ],

        'groups' => 'Groups',
        'select_group' => 'Select Group',
        'select_permission' => 'Select Permission',
        'permissions' => 'Permissions',
        'all_permissions' => 'All Permissions',
        'id' => 'ID',
        'name' => 'Name',
        'slug' => 'Slug',
        'action' => 'Action',
        'delete' => 'Delete',
        'alert' => 'Are You Sure To Delete Permission From Selected Group ?',
        'table_header' => 'Permissions',
        'sorry' => 'Oops !!',
        'you_have_not_permission' => 'Sorry You\'re Not Authorised To Open This Page',
        'delete_all' => 'Delete All',
        'confirm_delete' => 'Confirm Delete',
        'delete_modal' => 'Delete Permissions',
        'delete_selected' => 'Delete Selected',
        'close' => 'Close',
        'permission' => 'Permission',
        'permission_s' => 'Permissions',
        'select_for_delete' => 'Please Select Permission',
        'must_select' => 'You Must Select At least Permission To Delete',
    ],

    'branch'=>[
        'buttons'=>[
            'add_branch' => 'Add Branch',
            'branch_setting' => 'Branch Settings',
            'branch_sales_target' => 'Add Branch Sales Target',
            'edit_branch' => 'Edit Branch Info',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'setting' => 'Branch Settings',
        'location' => 'Location Setup',
        'completed' => 'Review information',
        'company_details' => 'Branch Details',
        'name' => 'Name',
        'code' => 'Code',
        'desc' => 'Description',
        'manage' => 'Managed By',
        'email' => 'Email',
        'mobile' => 'Mobile',
        'landline' => 'Landline',
        'website' => 'Website',
        'company' => 'Choose Company',
        'active' => 'Active',
        'address' => 'Address',
        'address_span' => 'Please Insert Address',
        'country' => 'Country',
        'select_country' => 'Choose Country',
        'state' => 'State',
        'select_state' => 'Choose State',
        'area' => 'Area',
        'select_area' => 'Choose Area',
        'table_header' => 'Branches',
        'table_header_desc' => 'Branches Employees',
    ],
    'general_setting'=>[
        'buttons'=>[
            'add_branch' => 'Add Branch',
            'branch_setting' => 'Branch Settings',
            'branch_sales_target' => 'Add Branch Sales Target',
            'edit_branch' => 'Edit Branch Info',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'setting' => 'Branch General Setting',
        'add_setting' => 'Add General Setting To Branch',
        'sales_invoice_print_size' => 'Print Size Sales Invoice',
        'sales_quotation_print_size' => 'Print Size Quota Invoice',
        'sales_invoice_message' => 'Sales Invoice Message',
        'sales_quotation_message' => 'Quota Invoice Message',
        'barcode_width' => 'Barcode Width',
        'barcode_height' => 'Barcode Height',
        'id'=>'ID',
        'company'=>'Company',
        'branch' => ' Branch',
        'created_by'=>'Created By',
        'action'=>'Actions',
        'active' => 'Active',
        'table_header' => 'Settings',
        'table_header_desc' => 'General Settings of Branch',
    ],
    'supplier'=>[
        'buttons'=>[
            'add_supplier' => 'Add Suppliers',
            'supplier_setting' => 'Suppliers Settings',
            'edit_supplier' => 'Edit Supplier Info',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'setting' => 'Supplier Settings',
        'location' => 'Location Setup',
        'completed' => 'Review Info',
        'company_details' => 'Supplier Details',
        'name' => 'Name',
        'code' => 'Code',
        'desc' => 'Description',
        'contact_person' => 'Contact Person',
        'payment_terms' => 'Payment Terms',
        'currency' => 'Currency',
        'email' => 'Email',
        'mobile' => 'Mobile',
        'landline' => 'Landline',
        'website' => 'Website',
        'company' => 'Choose Compay',
        'active' => 'Active',
        'address' => 'Address',
        'address_span' => 'Please Insert Address',
        'country' => 'Country',
        'select_country' => 'Choose Country',
        'state' => 'State',
        'select_state' => 'Choose State',
        'area' => 'Area',
        'select_area' => 'Choose Area',
        'table_header' => 'Suppliers',
        'table_header_desc' => 'Suppliers Info',
    ],

    'branch_target'=>[
        'buttons'=>[
            'add_branch_target' => 'Add Branch Target',
            'branch_target_setting' => 'Branch Target Settings',
            'branch_sales_target' => 'Branch Sales Target',
            'edit_branch_target' => 'Edit Branch Target',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'setting' => 'Branch Target Settings',
        'completed' => 'Review Info',
        'company_details' => 'Branch Target Details',
        'name' => 'Name',
        'month' => 'Month',
        'target' => 'Target',
        'branch' => 'Choose Branch',
        'company' => 'Choose Company',
        'active' => 'Active',
        'table_header' => 'Targets',
        'table_header_desc' => 'Branches Targets :',
    ],
    'currency'=>[
        'buttons'=>[
            'add_currency' => 'Add Co. Currencies',
            'edit_currency' => 'Edit Co Currencies Info',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
            'default' => 'Default Currency ',
        ],
        'setting' => 'Co. Currency Settings',
        'completed' => 'Review Info',
        'currency_details' => 'Co. Currency Details',
        'name' => 'Name',
        'symbol' => 'Symbol',
        'symbol_position' => 'Symbol Position',
        'select_symbol_position' => ' Choose Symbol Position',
        'company' => 'Choose Company',
        'active' => 'Active',
        'table_header' => 'Currencies',
        'table_header_desc' => 'Co. Currencies :',
    ],
    'brands'=>[
        'buttons'=>[
            'add_brand' => 'Add Brands',
            'edit_brand' => 'Edit Brand',
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'setting' => 'Co. Brand Settings',
        'completed' => 'Review Info',
        'brand_details' => 'Co. Brand Details',
        'name' => 'Name',
        'company' => 'Choose Company',
        'active' => 'Active',
        'table_header' => 'Brands',
        'table_header_desc' => 'Co. Brands  :',
    ],
    'inventory'=>
        [
            'defaults'=>[
                'back' => 'Back',
                'next' => 'Next',
                'previous' => 'Previous',
                'submit' => 'Submit',
            ],
            'category'=>[
                'cat' => 'Category',
                'select_cat' => 'Select Category',
                'cats' => 'Categories',
                'product_cats' => 'Product Category',
                'add_cats' => 'Add Category',
                'setting' => 'Product Category Settings',
                'cat_details' => 'Edit Product Category',
                'name' => 'Name',
                'code' => 'Code',
                'parent' => 'Prent',
                'edit_status' => 'Edit Status',
                'desc' => 'Description',
                'choose_cat' => 'Choose Product Category',
                'main_cat' => 'Main Category',
                'sub_cat' => 'Sub Category',
//                'cat' => 'Category',
                'active' => 'Active',
                'table_header' => 'Product Categories',
                'table_header_desc' => 'Product Categories Co. :',
                'add' => 'Add',
                'description' => 'Description',
                'title' => 'Title',
                'attached_file' => 'Attached File'

            ],
            'item_units'=>[
                'item_units' => 'Item Units',
                'add_item_units' => 'Add Item Units',
                'edit' => 'Edit Item Unit',
                'delete' => 'Deactivate Item Unit',
                'setting' => 'Item Unit Settings',
                'item_units_details' => 'Item Unit Details',
                'name' => 'Name',
                'abbreviation' => 'Abbreviation',
                'decimal' => 'Decimal',
                'decimal_span' => 'Decimal Number  (from 0 to 4)',
                'active' => 'Active',
                'table_header' => 'Item Units',
                'table_header_desc' => 'Item Units Co. :',
            ],

            'items'=>[
                'items' => 'Items',
                'add_items' => 'Add New Item',
                'edit' => 'Edit Item',
                'delete' => 'Deactivate Item',
                'setting_own' => 'Item Owned Setting',
                'setting_identification' => 'Item Identification Setting',
                'setting_finance' => 'Item Finance Setting',
                'items_details_1' => 'Determine Item Owner',
                'item_branch' => 'Branch',
                'item_main_cat' => 'Main-Category',
                'item_sub_cat' => 'Sub-Category',
                'item_type' => 'Type',
                'item_unit' => 'Unit',
                'items_details_2' => 'Item Details',
                'image_url' => 'Image',
                'name' => 'Name',
                'serial_no' => 'Serial No.',
                'sku' => 'SKU',
                'desc' => 'Description',
                'brand' => 'Brand',
                'color' => 'Color',
                'items_price_details' => 'Item Finance Settings',
                'cost_price' => 'Cost Price',
                'retail_price' => 'Retail Price',
                'wholesale_price' => 'Wholesale Price',
                'tax_percentage' => 'Tax (%)',
                'discount_percentage' => 'Discount (%)',
                'quantity' => 'Quantity',
                'exclude_from_sale' => 'Exclude From Sale',
                'exclude_from_purchase' => 'Exclude From Purchase',
                'reorder_level' => 'Reorder Level',
                'abbreviation' => 'Abbreviation',
                'decimal' => 'Decimal',
                'decimal_span' => 'Decimal Number  (from 0 to 4)',
                'active' => 'Active',
                'edit_qty_settings' => 'Edit Quantity Settings',
                'choose_item' => 'Choose Item',
                'current_qty' => 'Current Quantity',
                'edit_qty' => 'Edit Quantity',
                'pending_transfer_item' => 'Pending Transfer Items',
                'log_type' => 'Type',
                'status' => 'Active',
                'status_off' => 'Inactive',


            ],

            'item_transation'=>[
                'transatioms' => 'Item Transition',
                'add_items' => 'Add New Item',
                'edit' => 'تعديل وحدة الصنف',
                'delete' => 'تعطيل وحدة الصنف',
                'setting' => 'اعدادات نقل اصناف',
                'transations_details' => 'Item Transition Details From Branch',
                'items' => 'Available Items in Branch',
                'choose_items' => 'Please Select Item',
                'branches' => 'Branches',
                'choose_branch' => 'Please Select Branch',
                'current_qty' => 'Available Quantity',
                'transation_qty' => 'Transition Quantity From',
                'add' => 'Add Another Item',
                'items_transitioned' => 'Incoming Items',
                'new_item_transfer' => 'Item Transition',

            ],
            'sales_kit'=>[
                'sales_kit_details' => 'Add Sales Kit Details',
                'sales_kit_contents' => 'Sales Kit Contents',
                'add_sales_kit' => 'Add New Sales Kit',
                'edit' => 'Edit',
                'delete' => 'Delete',
                'setting' => 'اعدادات نقل اصناف',
                'name' => 'Kit Name',
                'code' => 'Kit Code',
                'desc' => 'Kit Description',
                'unit_sell_price' => 'Unit Sell Price',
                'sales_kit' => 'Sales Kits',
                'sales_kit_items' => 'Sales Kit Items',
                'transation_qty' => 'Items Quantity',

            ],
            'item_discount'=>[
                'item_discount_details' => 'Add Item Discount',
                'add_item_discount' => 'Add Item Discount',
                'main_cat' => 'Main Category',
                'select_main_cat' => 'Select Main Category',
                'sub_cat' => 'Sub Category',
                'select_sub_cat' => 'Select Sub Category',
                'items' => 'Items',
                'all' => 'All',
                'select_items' => 'Select Item',
                'item_discount' => 'Discount Percentage (%)',
            ],
            'purchase_invoice'=>[
                'purchase_invoices' => 'Purchase Invoices',
                'ADD_purchase_invoices' => 'Add Purchase Invoice',
                'suppliers' => 'Supplier',
                'select_supplier' => 'Please Select Supplier',
                'item_name' => 'Item Name',
                'item' => 'Select Items to Purchase',
                'qty' => 'Quantity',
                'unit_price' => 'Unit Price',
                'tax' => 'Tax (%)',
                'discount' => 'Discount',
                'total_amount' => 'Total Amount',
                'allocated_amount' => 'Allocated Amount ',
                'action' => 'Action',
                'payment_terms' => 'Payment Terms',
                'choose_payment_terms' => 'Please Select Payment Term',
                'table_header_desc' => 'Purchase Invoices in Your Own Company',
                'branch' => ' Branch',
                'show' => 'Show Invoice Details',
                'note' =>'Note',
                'edit'=>'Edit',
                'delete'=>'Delete',
                'active'=>'Active',
                'purchase_invoice' => 'Purchase Invoice',
                'company' => 'Company',
                'date' => 'Invoice Date',
                'id_no' => 'Invoice No.',
                'supplier' => 'Supplier',
                'item_id' => 'Item Id',
                'item_qty' => 'Quantity',
                'item_price' => 'Unit Price',
                'supplier_transaction' => 'Supplier Transaction',
                'transaction_date' => 'Transaction Date',
                'download' => 'Download',
            ],
            'sales_invoice'=>[
                'sales_invoices' => 'Sales Invoices',
                'ADD_sales_invoices' => 'Add Sales Invoice',
                'customers' => 'Customers',
                'select_customer' => 'Please Select Customer',
                'item_name' => 'Item Name',
                'item' => 'Please Select Item',
                'qty' => 'Quantity',
                'unit_price' => 'Unit Price',
                'tax' => 'Tax(%)',
                'discount' => 'Discount',
                'total_amount' => 'Total Amount',
                'actions' => 'Actions',
                'payment_terms' => 'Payment Terms',
                'choose_payment_terms' => 'Please Select Payment Term',
//                'table_header'=> 'أوامر التصنيع',
//                'table_header_desc' => 'فواتير البيع الخاصة بشركتكم',
                'id'=>'ID',
                'created_by'=>'CREATED_BY',
                'tax_amount'=>'TAX',
                'amount_without_tax'=>'Amount With NO Tax',
                'discount_amount'=>'Total Discount',
                'branch' => 'BRANCH',
                'show' => 'Show Invoice Details',
                'note' =>'Notes',
                'edit'=>'Edit',
                'delete'=>'Delete',
                'active'=>'Active',
                'sales_invoice' => 'Sales Invoice',
                'company' => 'Company',
                'date' => 'Invoice Date',
                'id_no' => 'Invoice No.',
                'customer' => 'Customer',
                'item_id' => 'ITEM ID',
                'item_qty' => 'QUANTITY',
                'item_price' => 'UNIT PRICE',
                'customer_transaction' => 'CUSTOMER TRANSACTION',
                'transaction_date' => 'TRANSACTION DATE',
                'download' => 'DOWNLOAD',
                'invoice_price_type' => 'Select Invoice Price Type',
                'retail_price' => 'Retail invoice',
                'wholesale_price' => 'Wholesale Invoice',
                'search_item' => 'Please Enter Item Name or Barcode',
                'selected_item' => 'Item',
                'Grand_total' => 'Total Invoice',
                'barcode' => 'Barcode',
                'select_barcode' => 'Insert Barcode',
                'allocated_amount' => 'Allocated Amount Of Customer',
            ],
            'return_item'=>[
                'return_item' => 'Return Item',
                'suppliers' => 'Suppliers',
                'select_supplier' => 'Please Select Supplier',
                'item_name' => 'Item Name',
                'qty' => 'Quantities Need Return',
                'unit_prices' => 'Unit Prices',
                'select_unit_price' => 'Please Select Unit Price',
            ],
        ],
    'manufacturing'=>[
        'defaults'=>[
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'bom'=>[
            'BOM' => 'Bill of Material',
            'BOM_items' => 'Bill of Material Items',
            'add_BOM' => 'Add Bill of Material',
            'BOM_details' => 'Bill of Material Details',
            'BOM_details_contents' => 'Bill of Material Contents',
            'choose_BOM' => 'Choose Bill of Material',
            'choose_BOM_items' => 'Choose Bill of Material Items',
            'BOM_item_qty' => 'BOM-item Quantity',
            'table_header'=> 'Bill of Materials',
            'edit'=>'Edit',
        ],
        'cost_center'=>[
            'add_CC' => 'Add Cost Center Details',
            'CC_details' => 'Cost Centers Details',
            'CC_name' => 'Cost Center Name',
            'table_header'=> 'Cost Centers',
            'edit'=>'Edit',
            'delete'=>'Deactivate',
            'active'=>'Active',
        ],
        'work_order'=>[
            'BOM' => 'Manufactured Items',
            'BOM_items' => 'Bill of Material Items',
            'add_CC' => 'Add Cost Center',
            'add_work_order' => 'Add Work Order',
            'add_wo_details' => 'Add Work Order Details',
            'item' => 'Choose Manufacturing Item',
            'qty' => 'Quantities to manufacturing',
            'type' => 'Choose Work Order Type',
            'cost_center' => 'Choose Cost Center',
            'target_date' => 'Target Date',
            'details' => 'Work Order Details',
            'cost_status_details' => 'Costs and Status Details',
            'status' => 'Next Work Order Status',
            'current_status' => 'Current Work Order Status',
            'choose_status' => 'Choose Next Work Order Status',
            'final_qty' => 'Final Quantity',
            'final_date' => 'Final Date',
            'costs_details_contents' => 'Work Order Costs Details',
            'add_cost_details' => 'Add New Work Order Cost',
            'choose_cost_type' => 'Choose Cost Type',
            'cost_value' => 'Cost Value',
            'choose_BOM_items' => 'Choose Bill of Material Item',
            'BOM_item_qty' => 'Items Quantity',
            'init_balance' => 'الرصيد المبدأي',
            'balance' => 'الرصيد',
            'table_header'=> 'Work Orders',
//            'table_header_desc' => 'إدارة الحسابات الخاصة بشركتكم',
            'branch' => ' Branch',
            'note' =>'Note',
            'edit'=>'Edit',
            'delete'=>'Deactivate',
            'active'=>'Activate',
            'id'=>'ID',
            'cost_center_record'=>'Cost Center',
            'item_name'=>'Item',
            'quantity'=>'Qty',
            'created_by'=>'Created By',
            'created_at'=>'Created At',
            'type_record'=>'Type',
            'status_record'=>'Status',
            'actions'=>'Actions',
            'created_date'=>'Select Created Date',
            'from'=>'From',
            'to'=>'To',
            'work_order_type_select'=>'Select Work Order Type',
            'work_order_type'=>'Work Order Type',
            'work_order_status_select'=>'Select Work Order Status',
            'work_order_status'=>'Work Order Status',
            'cost_type'=>'Cost Order Type',
            'work_order_cost_table'=>'Work Order Costs (:item)',
            'work_order_req_table'=>'Work Order Requirements (:item)',
            'value'=>'Value',
            'unit_quantity'=>'Unit Qty',
            'total_quantity'=>'Total Qty',
            'unit_cost_price'=>'Unit Cost Price',
            'work_order_unit_cost'=>'Work Order Cost per Unit',
            'work_order_cost'=>'Work Order Costs',
            'total_unit_costs'=>'Raw Material Cost For Item Unit',
            'total_item_cost'=>'Total Raw Material Costs',
            'total_costs'=>'Total Cost',
            'costs_table'=>'Total Cost Details',
        ],
    ],
    'sales'=>[
        'buttons'=>[
            'back' => 'Back',
            'next' => 'Next',
            'previous' => 'Previous',
            'submit' => 'Submit',
        ],
        'customer_item_return'=>[
            'return_details'=>'Items Need To Return',
            'add_new_row'=>'Add New Row',
            'invoice'=>'Invoice',
            'invoice_search'=>'Insert Invoice ID',
            'choose_item' => 'Choose Item',
            'item' => 'Item',
            'qty' => 'Quantity',
            'insert_qty' => 'Quantity Need To Return',
            'id'=>'ID',
            'company'=>'Company',
            'customer'=>'Customer',
            'invoice_id'=>'Invoice ID',
            'total_amount'=>'Amount',
            'transaction_date'=>'Transaction Date',
            'created_by'=>'Created By',
            'action'=>'Actions',
            'table_header'=> 'Returned Items',
            'table_header_desc' => 'Returned Items in Company',
            'branch' => 'branch',
            'note' =>'Notes',
            'edit'=>'Edit',
            'delete'=>'Delete',
            'active'=>'Active',
            'details'=>'Details',
        ],
        'sales_quotation'=>[
            'sales_quotations' => 'Sales Quotations',
            'ADD_sales_quotations' => 'Add Sales Quotation',
            'customers' => 'Customers',
            'select_customer' => 'Please Select Customer',
            'item_name' => 'Item Name',
            'item' => 'Please Select Item',
            'unit_price' => 'Unit Price',
            'qty' => 'Quantity',
            'tax' => 'Tax(%)',
            'discount' => 'Discount',
            'total_amount' => 'Total Amount',
            'actions' => 'Actions',
            'payment_terms' => 'Payment Terms',
            'choose_payment_terms' => 'Please Select Payment Term',
            'table_header'=> 'Sales Quotations',
            'table_header_desc' => 'Sales Quotations in Your Company',
            'id'=>'ID',
            'created_by'=>'Created By',
            'tax_amount'=>'Tax Amount',
            'discount_amount'=>'Discount Amount',
            'amount_without_tax'=>'Total Amount With No Tax',
            'branch' => 'Branch',
            'show' => 'Show',
            'note' =>'Notes',
            'edit'=>'Edit',
            'delete'=>'Delete',
            'active'=>'Active',
            'sales_invoice' => 'Sales Invoice',
            'company' => 'Company',
            'barcode' => 'Barcode',
            'customer' => 'Customer',
            'select_barcode' => 'Please Insert Barcode',
            'expiration_date' => 'Expiration Date',
            'change_to_sales' => 'Change To Sales',
            'sales_kit' => 'Sales Kit',
            'sales_kit_qty' => 'Sales Kit Quantity',
            'details'=>'Details',
            'details_sub_table'=>'Sales Quotation Datails',
            'retail_price' => 'Retail Price',
            'wholesale_price' => 'Wholesale price',
            'invoice_price_type' => 'Select Price Type',

        ],

    ],
    'reports'=>[
        'sales_rep'=>[
            'sales_quotations' => 'عروض أسعار البيع',
            'ADD_sales_quotations' => 'أضافة عرض أسعار بيع',
            'all_sales_reps' => 'كل المندوبين',
            'sales_reps' => 'المندوبين',
            'select_sales_rep' => 'من فضلك أختر المندوب',
            'created_date' => 'تاريخ الإنشاء',
            'from_date' => 'من تـاريخ',
            'to_date' => 'إلى تـاريخ',
            'id'=>'الرقم التعريفي',
            'sales_rep_name'=>'أسم المندوب',
            'customer_name'=>'أسم العميل',
            'customer_address'=>'عنوان العميل',
            'customer_balance'=>'رصيد الحساب',
            'customer_table_head'=>'العملاء',
            'sales_invoice_id'=>'رقم فاتورة بيع',
            'transaction_date'=>'تـاريخ التحويل',
            'allocated_amount'=>'المبالغ المدفوعة',
            'customer_sales_invoice_table_head'=>'فواتير البيع',
            'customer_payment_table_head'=>'الدفعات المستلمة',
            'customer_return_table_head'=>'المرتجعات',
            'item_name' => 'أسم المنتج',
            'item' => 'أختر المنتج لمراد بيعه',
            'unit_price' => 'سعر الوحدة',
            'qty' => 'الكمية',
            'tax' => 'الضريبة(%)',
            'discount' => 'الخصم',
            'total_amount' => 'السعر الكلي',
            'actions' => 'الاجراءات',
            'payment_terms' => 'طريقة الدفع',
            'choose_payment_terms' => 'أختر طريقة الدفع',
            'table_header'=> 'عرض الاسعار',
            'table_header_desc' => 'عروض الاسعار الخاصة بشركتكم',
            'created_by'=>'المنشئ',
            'tax_amount'=>'إجمالي الضريبة',
            'discount_amount'=>'إجمالي الخصم',
            'amount_without_tax'=>'سعر الكلي بدون الضريبة',
            'branch' => ' الفرع',
            'show' => 'عرض تفاصيل العرض',
            'note' =>'ملاحظات',
            'edit'=>'تعديل',
            'delete'=>'تعطيل',
            'active'=>'تفعيل',
            'sales_invoice' => 'فاتورة بيع',
            'company' => 'الشركة',
            'barcode' => 'الكود',
            'customer' => 'العميل',
            'select_barcode' => 'أدخل البار الكود الخاص',
            'expiration_date' => 'تاريخ نهاية صلاحية العرض',
            'change_to_sales' => 'تحويل لفاتورة بيع',
            'sales_kit' => 'عرض مجمع',
            'sales_kit_qty' => 'كمية العرض',
            'details'=>'التفاصيل',
            'details_sub_table'=>'تفاصيل عرض الاسعار',
            'retail_price' => 'فاتورة بالتجزئة',
            'wholesale_price' => 'فاتورة بالجملة',
            'invoice_price_type' => 'أختر نوع الفاتورة',

        ],

        'items'=>[
            'out_of_stock' => 'Out Of Stock Items',
            'all' => 'All',
            'branches' => 'Branches',
            'select_branch' => 'Please Select Branch',
            'created_date' => 'Created Date',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'id'=>'ID',
            'item_name' => 'Item Name',
            'serial_no'=>'Serial No.',
            'cost_price'=>'Cost Price',
            'retail_price'=>'Retail Price',
            'qty' => 'Qty',
            'branch_name' => 'Branch Name',
            'created_by' => 'Created By',
            'items' => 'Items',
            'select_item' => 'Select Item',
            'import' => 'Purchased Items',
            'sells_return' => 'Return Sells',
            'purchase_return' => 'Return Purchase',
            'sells' => 'Sells',
            'transferred_to_branches' => 'Transferred To Branches',
            'ID' => 'ID',
            'item_log_type' => 'Statement',
            'invoice_no' => 'Invoice No',
            'qty_balance' => 'Balance',
            'new_entry' => 'New Entry',
            'qty_modification' => 'Qty Modification',

        ],

        'sales'=>[
            'most_sales_times' => 'Most Sales Times In :  ',
            'all' => 'All',
            'branches' => 'Branches',
            'select_branch' => 'Please Select Branch',
            'created_date' => 'Created Date',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'id'=>'ID',
            'hours'=>'Hour',
            'hours_system'=>'24 Hrs System',
            'sales'=>'Sales',
            'sales_in_hours'=>'Completed in this time',

        ],
    ],
    'crm'=>[
        'name' => 'Name',
        'desc' => 'Description',
        'status' => 'Status',
        'type' => 'Type',
        'createdAt' => 'Created At',
        'createdBy' => 'Created By',
        'male' => 'Male',
        'female' => 'Female',
        'tickets'=>[
            'created_at'=>'Created At',
            'from'=>'From',
            'to'=>'To',
            'all'=>'All',
            'updated_at'=>'Updated At',
            'categories'=>'Categories',
            'select_category'=>'Select Category',
            'statuses'=>'Statuses',
            'select_status'=>'Select Status',
        ],

        'crm_status_group'=>[
            'add_status_groups'=>'Add Status Groups',
            'created_at'=>'Created At',
            'from'=>'From',
            'to'=>'To',
            'all'=>'All',
            'updated_at'=>'Updated At',
            'groups'=>'Groups',
            'select_groups'=>'Select Groups',
            'statuses'=>'Statuses',
            'select_status'=>'Select Status',
            'table_header'=>'Status Groups',
            'table_header_desc'=>'For Company',
            'ID'=>'ID',
            'company'=>'Company',
            'group'=>'Group',
            'status_group'=>'Group Status',
            'status'=>'Status',
            'actions'=>'Actions',
            'active'=>'Active',
            'Inactive'=>'Inactive',
        ],

        'crm_group_users'=>[
            'add_status_groups'=>'Add Status Groups',
            'created_at'=>'Created At',
            'from'=>'From',
            'to'=>'To',
            'all'=>'All',
            'updated_at'=>'Updated At',
            'groups'=>'Groups',
            'select_groups'=>'Select Groups',
            'users'=>'Users',
            'select_users'=>'Select Users',
            'table_header'=>'Group Users',
            'table_header_desc'=>'For Company',
            'ID'=>'ID',
            'company'=>'Company',
            'group'=>'Group',
            'user'=>'User',
            'status'=>'Status',
            'actions'=>'Actions',
            'active'=>'Active',
            'Inactive'=>'Inactive',
        ],
    ],
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login'=>[
        'email' => 'البريد الالكتروني',
        'password' => 'كلمة السر',
    ],



];
