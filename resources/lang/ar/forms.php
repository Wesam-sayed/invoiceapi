<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'buttons'=>[
        'choose_option'=>'أختر خيارا',
    ],
    'form_header'=>[
        'account'=>'بيانات المستخدم',
        'about_user'=>'بيانات شخصية',
        'add_category' => 'اضافه تصنيف',
        'add_status' => 'اضافه حاله',
        'statuses' => 'الحالات',
        'user_profile'=>'بيانات البروفايل',
        'user_groups'=>'بيانات الجروب / الشركة',
        'user_credentials'=>'بيانات الخاصة للمستخدم',
    ],

    'inventory'=>
        [
            'defaults'=>[
                'back' => 'رجوع',
                'next' => 'التالي',
                'previous' => 'رجوع',
                'submit' => 'تأكيد',

            ],


            'sales_invoice'=>[
                'sales_invoices' => 'فواتير البيع',
                'ADD_sales_invoices' => 'أضافة فاتورة بيع',
                'customers' => 'العملاء',
                'select_customer' => 'من فضلك أختر العميل',
                'add_work_order' => 'أضافة أمر تصنيع',
                'add_wo_details' => 'أضافة تفاصيل أمر التصنيع',
                'item_name' => 'أسم المنتج',
                'item' => 'أختر المنتج لمراد بيعه',
                'unit_price' => 'سعر الوحدة',
                'qty' => 'الكمية',
                'tax' => 'الضريبة(%)',
                'discount' => 'الخصم',
                'total_amount' => 'الاجمالي',
                'actions' => 'الاجراءات',
                'payment_terms' => 'طريقة الدفع',
                'choose_payment_terms' => 'أختر طريقة الدفع',
                'table_header'=> 'أوامر التصنيع',
                'table_header_desc' => 'فواتير البيع الخاصة بشركتكم',
                'id'=>'الرقم التعريفي',
                'created_by'=>'المنشئ',
                'tax_amount'=>'إجمالي الضريبة',
                'discount_amount'=>'إجمالي الخصم',
                'amount_without_tax'=>'سعر الكلي بدون الضريبة',
                'branch' => ' الفرع',
                'show' => 'التفاصيل',
                'note' =>'ملاحظات',
                'edit'=>'تعديل',
                'delete'=>'تعطيل',
                'active'=>'تفعيل',
                'sales_invoice' => 'فاتورة بيع',
                'company' => 'الشركة',
                'date' => 'التاريخ',
                'id_no' => 'الرقم',
                'customer' => 'العميل',
                'item_id' => 'رقم المنتج',
                'item_qty' => 'الكمية',
                'item_price' => 'سعر المنتج',
                'customer_transaction' => 'حركة العميل',
                'transaction_date' => 'تاريخ الحركة',
                'download' => 'تحميل',
                'invoice_price_type' => 'أختر نوع الفاتورة',
                'retail_price' => 'فاتورة بالتجزئة',
                'wholesale_price' => 'فاتورة بالجملة',
                'search_item' => 'أدخل أسم النتج او الكود الخاص به',
                'selected_item' => 'المنتج',
                'Grand_total' => 'المبلغ الإجمالي',
                'barcode' => 'الكود',
                'select_barcode' => 'أدخل البار الكود الخاص',
                'allocated_amount' => 'المبالغ المسدده',
                'paid_amount'=>'الميلغ المدفوع',
                'net_amount' => 'المبلغ المتبقي',
                'customer_data' => 'بيانات العميل',
                'sell_time' => 'وقت البيع',
                'seller_name' => 'اسم البائع',
                'mobile' => 'الموبايل',
                'address' => 'العنوان',
                'telephone' => 'رقم التليفون',
                'print_invoice' => 'طباعة الفاتورة',
                'invoice' => 'بيان اسعار',
                'item_count' => 'عدد المنتجات',
                'total_amount_without_tax' => 'الاجمالي قبل الضرائب ',
                'tax' => 'ضريبة القيمة المضافة',
                'total_discounts' => 'إجمالي الخصومات',
                'withholding_tax_percentage' => 'ضريبة الخصم والضافة',
                'excise_tax_percentage' => 'ضريبة الجدول',
                'purchase_tax_percentage' => 'ضريبة المشتريات',
                'total_taxes' => 'إجمالي الضرائب',

            ],

        ],
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login'=>[
        'email' => 'البريد الالكتروني',
        'password' => 'كلمة السر',
    ],


];
