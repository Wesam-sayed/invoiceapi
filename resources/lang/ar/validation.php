<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ' :attribute يجب ان يكون مقبول.',
    'active_url'           => ' :attribute رابط غير صيحيح.',
    'after'                => ' :attribute يجب ان يكون بعد :date.',
    'after_or_equal'       => ' :attribute يجب ان سكون بعد او مساوى لـ :date.',
    'alpha'                => ' :attribute يجب ان يحتوى على حروف فقط.',
    'alpha_dash'           => ' :attribute يجب ان يحتوى على حروف و ارقام وعلامات.',
    'alpha_num'            => ' :attribute يجب ان يحتوى على حروف و ارقام.',
    'array'                => ' :attribute يجب ان يكون عباره عن مصفوفه.',
    'before'               => ' :attribute يجب ان يكون قبل :date.',
    'before_or_equal'      => ' :attribute يجب ان يكون قبل او مساوى لـ :date.',
    'between'              => [
        'numeric' => ' :attribute يجب ان يكون بين :min و :max.',
        'file'    => ' :attribute يجب ان يكون بين :min و :max كيلو بايت.',
        'string'  => ' :attribute يجب ان يكون بين :min و :max حرف.',
        'array'   => ' :attribute يجب ان يكون بين :min و :max وحده.',
    ],
    'boolean'              => ' :attribute يجب ان يكون صح او خطأ.',
    'confirmed'            => ' :attribute التأكيدغير مطابق.',
    'date'                 => ' :attribute تاريخ غير صحيح.',
    'date_format'          => ' :attribute غير مطابق للتنسيق الصحيح :format.',
    'different'            => ' :attribute و :other يجب ان يكونا مختلفين.',
    'digits'               => ' :attribute يجب ان يكون :digits ارقام.',
    'digits_between'       => ' :attribute يجب ان يكون بين :min و :max ارقام.',
    'dimensions'           => ' :attribute ابعاد الصورة غير صحيحة.',
    'distinct'             => ' :attribute هذا القيمة موجودة من قبل.',
    'email'                => ' :attribute يجب ان سكون بريد الكترونى صحيح.',
    'exists'               => ' تم اختيار :attribute وهو غير صحيح.',
    'file'                 => ' :attribute يجب ان  يكون ملف.',
    'filled'               => ' :attribute يجب ان يحتوى على قيمة.',
    'image'                => ' :attribute يجب ان يكون صورة.',
    'in'                   => ' تم اختيار :attribute وهو غير صحيح.',
    'in_array'             => ' :attribute غير موجود فى :other.',
    'integer'              => ' :attribute يجب ان يكون عدد.',
    'ip'                   => ' :attribute يجب ان يكون IP address صحيح.',
    'ipv4'                 => ' :attribute يجب ان يكون IPv4 address صحيح.',
    'ipv6'                 => ' :attribute يجب ان يكون IPv6 address صحيح.',
    'json'                 => ' :attribute يجب ان يكون JSON صحيح.',
    'max'                  => [
        'numeric' => ' :attribute يجب ان يكون اقل من :max.',
        'file'    => ' :attribute يجب ان يكون اقل من :max كيلوبايت.',
        'string'  => ' :attribute يجب ان يكون اقل من :max حرف.',
        'array'   => ' :attribute يجب ان يكون اقل من :max وحدة.',
    ],
    'mimes'                => ' :attribute يجب ان يكون ملف من نوع: :values.',
    'mimetypes'            => ' :attribute يجب ان يكون ملف من نوع: :values.',
    'min'                  => [
        'numeric' => ' :attribute يجب ان يكون على الاقل :min.',
        'file'    => ' :attribute يجب ان يكون على الاقل :min كيلوبايت.',
        'string'  => ' :attribute يجب ان يكون على الاقل :min حرف.',
        'array'   => ' :attribute يجب ان يكون على الاقل :min وحدة.',
    ],
    'not_in'               => 'تم اختيار :attribute وهو غير صحيح.',
    'numeric'              => ' :attribute يجب ان يكون رقم.',
    'present'              => ' :attribute يجب ان يكون حالى.',
    'regex'                => ' :attribute تنسيق غير صحيح.',
    //'required'             => ' :attribute هذا الحقل مطلوب.',
    'required'             => '  هذا الحقل مطلوب.',
    'required_if'          => ' :attribute مطلوب فى حالة :other تكون :value.',
    'required_unless'      => ' :attribute مطلوب لو لم  :other يكون :values.',
    'required_with'        => ' :attribute مطلوب عندما :values is present.',
    'required_with_all'    => ' :attribute مطلوب عندما  :values تكون حاضر.',
    'required_without'     => ' :attribute مطلوب عندما :values لا تكون حاضر.',
    'required_without_all' => ' :attribute مطلوب عندما لا تكون واحده من  :values حاضر.',
    'same'                 => ' :attribute و :other يجب ان يكونا متطابقين.',
    'size'                 => [
        'numeric' => ' :attribute يجب ان يكون :size.',
        'file'    => ' :attribute يجب ان يكون :size كيلوبايت.',
        'string'  => ' :attribute يجب ان يكون :size حرف.',
        'array'   => ' :attribute يجب ان يحتوى  :size وحدة.',
    ],
    'string'               => ' :attribute يجب ان يكون حرف.',
    'timezone'             => ' :attribute يجب ان يكون منطقة صحيحة.',
    'unique'               => ' :attribute تم دخول هذه القيمه من قبل.',
    'uploaded'             => ' :attribute فشل فى رفع الملف.',
    'url'                  => ' :attribute التنسيق غير صحيح.',

    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],




    'success' =>'العمليه تمت بنجاح',
    'confirm' =>'هل انت متأكد',
    'confirmCancel' => 'تم الإلغاء',
    'error' =>'حدث خطأ اثناء تنفيذ العمليه',
    'no_enough_balance' =>'رصيد الحساب غير كافي',
    'transfer_quantity_should_be_positive' =>'الكميه لابد ان تكون اكبر من صفر',
    'not_enough_quantity_from' =>'الكميه المطلوبه من ( :Name ) غير كافيه',
    'confirm'=>'هل انت متأكد',
    'cancelConfirm' =>'تم الإلغاء',
    'confirm_final_status' =>'هل أنت متأكد من إجراء أخر خطوة في أمر التصنيع ؟',
    'items_required_to_order_not_enough' =>'الكميات الموجودة بالمخزن غير سامحة للتنفيذ',
    'final_quantity_not_equal_needed' =>'الكمية التي تم تنفيذها لا تساوي الكمية المطلوبة برجاء إستكمال الكمية المطلوبة',
    'no_default_sales_account' => 'لا يوجد حساب مفعل للمبيعات',
    'returned_item_qty_bigger_than_exist' => 'الكمية المرتجعة أكبر من المباعة',
    'no_enough_balance_in_default_expenses_account' => 'لا يوجد رصيد كافي في الحساب الافتراضى للمصروفات',
    'no_default_expenses_account' => 'لا يوجد حساب مفعل للمصروفات',
    'validataionError' => 'من فضلك تأكد من صحة البيانات',
    'all_fields_required' => 'كل البيانات مطلوبة',
    'inserted_not_enough_quantity' => 'الكميه المطلوبه غير كافيه',
    'branch_item_not_enough_quantity' => 'الكميه الموجودة بالفرع غير كافية ولكن سيتم اجراء الطلب بناء على اعدادات الفرع',
    'transfer_logic_already_exists' => 'هذه العمليه موجوده بالفعل',
    'sales_price_smaller_than_mark_up' => 'سعر بيع ( :Name ) اقل من هامش الربح ',
    'item_option_value_added_before' => 'هذا النوع من الخيارات من منتج ( :Name ) تم إضافته من قيل',
    'sub_item_not_enough_for_main_item' => 'كمية المنتجات المكونة لهذا المنتج ( :Name ) غير كافية',

];
