<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login'=>[
        'email' => 'البريد الالكتروني',
        'password' => 'كلمة السر',
    ],

    'sales_invoice'=>[
        'title'=>'حالة فواتير المبيعات في الفرع',
        'daily_sales'=>'المبيعات اليومية',
        'weekly_sales'=>'المبيعات الاسبوعية',
        'monthly_sales'=>'المبيعات الشهرية',
        'yearly_sales'=>'المبيعات السنوية',
    ],
    'sales_target'=>[
        'title'=>'حالة تحقيق أهداف المبيعات لشهر ( :Name )',
        'branch_target'=>'هدف مبيعات الفرع الشهر الحالي',
        'total_sales_amount'=>'إجمالي مبيعات الفرع حتر الان',
    ],

    'trends'=>[
        'latest_items'=>'أحدث المنتجات المضافة',
    ],

    'latest_invoices'=>[
        'latest_invoices_added'=>'أحدث فواتير البيع المضافة',
        'Day'=>'اليوم',
        'Week'=>'الاسبوع',
        'Month'=>'الشهر',
    ],
    'highest_customer_purchased_this_month'=>'أكثر العملاء شراء في فرع (:branch) لشهر (:month)',
    'highest_users_sales_this_month'=>'أكثر البائيعين بيعا في فرع (:branch) لشهر (:month)',
    'total'=>'الاجمالي',
    'discount'=>'الخصم',
    'tax'=>'الضريبة',
    'total_daily_sales'=>'إجمالي المبيعات اليوم',
    'total_monthly_sales'=>'إجمالي المبيعات الشهر',
    'total_yearly_sales'=>'إجمالي المبيعات السنة',
    'total_no_sales_invoices'=>'عدد فواتير البيع  لشهر (:month)',
    'total_no_branch_customers'=>'عدد العملاء في الفرع',
    'total_no_open_tickets'=>'عدد السجلات المفتوحة خلال شهر (:month)',

];
