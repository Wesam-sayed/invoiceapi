<!--begin::Header-->
<div id="kt_header" class="header flex-column header-fixed">
    <!--begin::Top-->
    <div class="header-top">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Left-->
            <div class="d-none d-lg-flex align-items-center mr-3" id="kt_header_top_navs">
                <!--begin::Logo-->
                <a href="/" class="mr-20" id="app_logo">
                    <img alt="Logo" src="{{asset('assets/media/logos/logo-letter-1.png')}}" class="max-h-35px" />
                </a>
{{--            @php--}}
{{--                $collection = session('roles');--}}
{{--                $userHasGroup = session('group_id');--}}
{{--            @endphp--}}
            <!--end::Logo-->
                <!--begin::Tab Navs(for desktop mode)-->
{{--                <ul class="header-tabs nav align-self-end font-size-lg" role="tablist">--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','main-page') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_1" role="tab">{{ __('topbar.main_page') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','settings') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">{{ __('topbar.setting') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_3" role="tab">{{ __('topbar.INVENTORY') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_4" role="tab">{{ __('topbar.ACCOUNTING') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_5" role="tab">{{ __('topbar.CRM') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                    @endif--}}
{{--                    @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','manufacturing-header') || $userHasGroup == 0)--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_6" role="tab">{{ __('topbar.MANUFACTURING') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                    @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-header') || $userHasGroup == 0)--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_7" role="tab">{{ __('topbar.SALES') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                    @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','reports-header') || $userHasGroup == 0)--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_8" role="tab">{{ __('topbar.REPORTS') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}

{{--                    <li class="nav-item mr-3">--}}
{{--                        <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_9" role="tab">{{ __('topbar.HR') }}</a>--}}
{{--                    </li>--}}
{{--                    <!--end::Item-->--}}
{{--                </ul>--}}
                <!--begin::Tab Navs-->
            </div>
            <!--end::Left-->
            <!--begin::Topbar-->
            <div class="topbar bg-primary">



                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-icon btn-hover-transparent-white btn-dropdown btn-lg mr-1 pulse pulse-white">
												<span class="svg-icon svg-icon-xl">
                                                        <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2020-12-28-020759/theme/html/demo2/dist/../src/media/svg/icons/Text/Align-auto.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M9.61764706,5 L8.73529412,7 L3,7 C2.44771525,7 2,6.55228475 2,6 C2,5.44771525 2.44771525,5 3,5 L9.61764706,5 Z M14.3823529,5 L21,5 C21.5522847,5 22,5.44771525 22,6 C22,6.55228475 21.5522847,7 21,7 L15.2647059,7 L14.3823529,5 Z M6.08823529,13 L5.20588235,15 L3,15 C2.44771525,15 2,14.5522847 2,14 C2,13.4477153 2.44771525,13 3,13 L6.08823529,13 Z M17.9117647,13 L21,13 C21.5522847,13 22,13.4477153 22,14 C22,14.5522847 21.5522847,15 21,15 L18.7941176,15 L17.9117647,13 Z M7.85294118,9 L6.97058824,11 L3,11 C2.44771525,11 2,10.5522847 2,10 C2,9.44771525 2.44771525,9 3,9 L7.85294118,9 Z M16.1470588,9 L21,9 C21.5522847,9 22,9.44771525 22,10 C22,10.5522847 21.5522847,11 21,11 L17.0294118,11 L16.1470588,9 Z M4.32352941,17 L3.44117647,19 L3,19 C2.44771525,19 2,18.5522847 2,18 C2,17.4477153 2.44771525,17 3,17 L4.32352941,17 Z M19.6764706,17 L21,17 C21.5522847,17 22,17.4477153 22,18 C22,18.5522847 21.5522847,19 21,19 L20.5588235,19 L19.6764706,17 Z" fill="#000000" opacity="0.3"/>
                                                            <path d="M11.044,5.256 L13.006,5.256 L18.5,19 L16,19 L14.716,15.084 L9.19,15.084 L7.5,19 L5,19 L11.044,5.256 Z M13.924,13.14 L11.962,7.956 L9.964,13.14 L13.924,13.14 Z" fill="#000000"/>
                                                        </g>
                                                    </svg><!--end::Svg Icon-->

                                                    <!--end::Svg Icon-->
												</span>
                            <span class="pulse-ring"></span>
                        </div>
                    </div>
                    <!--end::Toggle-->
                    <!--begin::Dropdown-->
                    <div class="card-toolbar">
                        <div class="dropdown dropdown-inline">
                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                                <!--begin::Naviigation-->
                                <ul class="navi changeLang">
                                    {{-- <li class="navi-header font-weight-bold py-5">
                                        <span class="font-size-lg">Select Language:</span>
                                        <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
                                    </li> --}}
                                    <li class="navi-separator mb-3 opacity-70"></li>
                                    <li class="navi-item" value="0" {{ (session()->get('locale') || app()->getLocale()) == 'en' ? 'selected' : '' }}>
                                        <a href="#" class="navi-link">
																				<span class="navi-icon">
                                                                                   <i> <img src="{{asset('assets/media/svg/flags/226-united-states.svg')}}" style="padding-left: 5px" max-width: 50%/></i>
																				</span>
                                            <span class="navi-text" style="margin-left:10px; padding-right: 10px">English</span>
                                        </a>

                                    </li>
                                    <li class="navi-item" value="1" {{ (session()->get('locale') || app()->getLocale()) == 'ar' ? 'selected' : '' }}>
                                        <a href="#" class="navi-link">
																				<span class="navi-icon" >
																					<img src="{{asset('assets/media/svg/flags/158-egypt.svg')}}"  max-width: 50%/>
																				</span>
                                            <span class="navi-text" style="margin-left:10px; padding-right: 10px">Arabic</span>
                                        </a>

                                    </li>

                                </ul>
                                <!--end::Naviigation-->
                            </div>
                        </div>

                    </div>

                    <!--end::Dropdown-->
                </div>


                <!--end::Quick panel-->

                <!--begin::User-->
                <div class="topbar-item">
                    <div class="btn btn-icon btn-hover-transparent-white w-sm-auto d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                        <div class="d-flex flex-column text-right pr-sm-3">
                            <span class="text-white opacity-50 font-weight-bold font-size-sm d-none d-sm-inline">{{ session('fname') }} {{ session('lname') }}</span>
                            <span class="text-white font-weight-bolder font-size-sm d-none d-sm-inline">{{ session('branche_name') }}</span>
                        </div>
                        <span class="symbol symbol-35">
												<span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-30">S</span>
											</span>
                    </div>
                </div>
                <!--end::User-->
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Top-->
    <!--begin::Bottom-->
    <div class="header-bottom">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Header Menu Wrapper-->
{{--            <div class="header-navs header-navs-left" id="kt_header_navs">--}}
{{--                <!--begin::Tab Navs(for tablet and mobile modes)-->--}}
{{--                <ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs" role="tablist">--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','main-page') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                            <a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_1" role="tab">{{ __('topbar.main_page') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}

{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','settings') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                            <a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">{{ __('topbar.setting') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}

{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                            <a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_3" role="tab">{{ __('topbar.INVENTORY') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}

{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                            <a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_4" role="tab">{{ __('topbar.ACCOUNTING') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}

{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_5" role="tab">{{ __('topbar.CRM') }}</a>--}}
{{--                        </li>--}}
{{--                        <!--end::Item-->--}}
{{--                @endif--}}
{{--                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','manufacturing-header') || $userHasGroup == 0)--}}
{{--                    <!--begin::Item-->--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_6" role="tab">{{ __('topbar.MANUFACTURING') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}

{{--                    @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-header') || $userHasGroup == 0)--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_7" role="tab">{{ __('topbar.SALES') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                    @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','reports-header') || $userHasGroup == 0)--}}
{{--                        <li class="nav-item mr-3">--}}
{{--                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_8" role="tab">{{ __('topbar.REPORTS') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}

{{--                    <li class="nav-item mr-3">--}}
{{--                        <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#kt_header_tab_9" role="tab">{{ __('topbar.HR') }}</a>--}}
{{--                    </li>--}}
{{--                    <!--end::Item-->--}}
{{--                </ul>--}}
{{--                <!--begin::Tab Navs-->--}}
{{--                <!--begin::Tab Content-->--}}

{{--                <div class="tab-content">--}}
{{--                    <!--begin::Tab Pane-->--}}
{{--                    <div class="tab-pane py-5 p-lg-0" id="kt_header_tab_1">--}}
{{--                    @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','dashboard') || $userHasGroup == 0)--}}
{{--                        <!--begin::Menu-->--}}
{{--                            <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">--}}
{{--                                <!--begin::Actions-->--}}
{{--                                <a href="{{route('home')}}" class="btn btn-light-success font-weight-bold mr-3 my-2 my-lg-0">Dashboard</a>--}}
{{--                                <!--end::Actions-->--}}
{{--                            </div>--}}
{{--                    @endif--}}
{{--                    <!--end::Menu-->--}}
{{--                    </div>--}}
{{--                    <!--begin::Tab Pane-->--}}
{{--                    <!--begin::Tab Pane-->--}}
{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_2">--}}

{{--                        <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   Users   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','users') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-user" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.users') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-users') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item user" aria-haspopup="true">--}}
{{--                                                        <a href="{{url('users')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.users') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-users') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-user" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('add_user')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_users') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Company   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Company') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-company" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.COMPANY') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-companies') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu company" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('companies.index')}}" class="menu-link menu-item-active" aria-haspopup="true">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.companies') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-companies') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-company" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('companies.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_company') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}


{{--                                --}}{{--   BRANCHES   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Branches') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-branch" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.BRANCHES') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-branches') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu branches" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('branches.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.branches') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-branches') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('branches.create')}}" class="menu-link add-branch">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_branch') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--   SUPPLIERS   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Suppliers') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-supplier" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.SUPPLIER') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-suppliers') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu suppliers" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('suppliers.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.suppliers') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-suppliers') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-supplier" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('suppliers.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_supplier') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}



{{--                                --}}{{--   Brands   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Brands') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-brand" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.Brands') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-brands') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu brands" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('brands.index',[urlencode(base64_encode(session('company')['id']) || $userHasGroup == 0)])}}" class="menu-link ">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.brands') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-brandss') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-brand" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('brands.create')}}" class="menu-link ">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_brand') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--   Currency   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Currencies') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-currency" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.Currency') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-currencies') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu currency" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('currencies.index',[urlencode(base64_encode(session('company')['id']) || $userHasGroup == 0)])}}" class="menu-link ">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.currency') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-currencies') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-currency" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('currencies.create')}}" class="menu-link ">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_currency') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   PERMISSIONS   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-permissions') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-permission" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.PERMISSION') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-permissions') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu item-permissions" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('permissions.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.ADD_EDIT_PERMISSIONS') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Groups') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>--}}
{{--                                                                                    <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                </g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.GROUPS') }}</span>--}}
{{--                                                            <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                            <ul class="menu-subnav">--}}
{{--                                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-groups') || $userHasGroup == 0)--}}
{{--                                                                    <li class="menu-item groups" aria-haspopup="true">--}}
{{--                                                                        <a href="{{route('groups.index')}}" class="menu-link">--}}
{{--                                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                                <span></span>--}}
{{--                                                                            </i>--}}
{{--                                                                            <span class="menu-text">{{ __('topbar.subheader.GROUPS') }}</span>--}}
{{--                                                                        </a>--}}
{{--                                                                    </li>--}}
{{--                                                                @endif--}}

{{--                                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-add-groups') || $userHasGroup == 0)--}}
{{--                                                                    <li class="menu-item add-group" aria-haspopup="true">--}}
{{--                                                                        <a href="{{route('groups.create')}}" class="menu-link">--}}
{{--                                                                            <i class="menu-bullet menu-bullet-dot">--}}
{{--                                                                                <span></span>--}}
{{--                                                                            </i>--}}
{{--                                                                            <span class="menu-text">{{ __('topbar.subheader.create_group') }}</span>--}}
{{--                                                                        </a>--}}
{{--                                                                    </li>--}}
{{--                                                                @endif--}}
{{--                                                            </ul>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--   General Settings   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','General-Settings') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-general-settings" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('common.print_settings') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-general-settings') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu general-settings" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('general_settings.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('common.print_settings') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-general-add-settings') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-general-settings" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('general_settings.create')}}" class="menu-link add-branch">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('common.add') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--   Notification Settings   --}}
{{--                                <li class="menu-item menu-item-submenu menu-item-rel menu-notification-settings" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                        <span class="menu-text">{{ __('topbar.subheader.notification_settings') }}</span>--}}
{{--                                        <span class="menu-desc"></span>--}}
{{--                                        <i class="menu-arrow"></i>--}}
{{--                                    </a>--}}
{{--                                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                        <ul class="menu-subnav">--}}
{{--                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-general-settings') || $userHasGroup == 0)--}}
{{--                                                <li class="menu-item menu-item-submenu notifications" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('notifications.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.notifications') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                            @endif--}}

{{--                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','control-panel-general-add-settings') || $userHasGroup == 0)--}}
{{--                                                <li class="menu-item menu-item-submenu add-notification" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('notifications.create')}}" class="menu-link add-branch">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.add_notification') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                            @endif--}}

{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--begin::Tab Pane-->--}}
{{--                    <!--begin::Tab Pane-->--}}
{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_3">--}}

{{--                        <div id="kt_header_menu_2" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   CATEGORIES   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Categories') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-cat" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.categories') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-categories') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('categories.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.categories') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-add-categories') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('categories.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_categories') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Item Units   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Units') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-units" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.item_units') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-units') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu item-units" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('item-units.index')}}" class="menu-link menu-item-active" aria-haspopup="true">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.item_units') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-add-units') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-item-units" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('item-units.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_item_units') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Items   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','Items') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-items" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.items') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-items') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu items" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('items.index')}}" class="menu-link menu-item-active" aria-haspopup="true">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.items') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-add-items') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-item" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('items.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_item') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-modify-item-quantity') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu edit-item-qty" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('item_branch_qty.edit')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.edit_item_qty') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-pending-sent-items') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu item-transfer-pending" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('items_transfer.pending')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <rect x="0" y="0" width="24" height="24"/>--}}
{{--                                                                                    <path d="M19.5,10.5 L21,10.5 C21.8284271,10.5 22.5,11.1715729 22.5,12 C22.5,12.8284271 21.8284271,13.5 21,13.5 L19.5,13.5 C18.6715729,13.5 18,12.8284271 18,12 C18,11.1715729 18.6715729,10.5 19.5,10.5 Z M16.0606602,5.87132034 L17.1213203,4.81066017 C17.7071068,4.22487373 18.6568542,4.22487373 19.2426407,4.81066017 C19.8284271,5.39644661 19.8284271,6.34619408 19.2426407,6.93198052 L18.1819805,7.99264069 C17.5961941,8.57842712 16.6464466,8.57842712 16.0606602,7.99264069 C15.4748737,7.40685425 15.4748737,6.45710678 16.0606602,5.87132034 Z M16.0606602,18.1819805 C15.4748737,17.5961941 15.4748737,16.6464466 16.0606602,16.0606602 C16.6464466,15.4748737 17.5961941,15.4748737 18.1819805,16.0606602 L19.2426407,17.1213203 C19.8284271,17.7071068 19.8284271,18.6568542 19.2426407,19.2426407 C18.6568542,19.8284271 17.7071068,19.8284271 17.1213203,19.2426407 L16.0606602,18.1819805 Z M3,10.5 L4.5,10.5 C5.32842712,10.5 6,11.1715729 6,12 C6,12.8284271 5.32842712,13.5 4.5,13.5 L3,13.5 C2.17157288,13.5 1.5,12.8284271 1.5,12 C1.5,11.1715729 2.17157288,10.5 3,10.5 Z M12,1.5 C12.8284271,1.5 13.5,2.17157288 13.5,3 L13.5,4.5 C13.5,5.32842712 12.8284271,6 12,6 C11.1715729,6 10.5,5.32842712 10.5,4.5 L10.5,3 C10.5,2.17157288 11.1715729,1.5 12,1.5 Z M12,18 C12.8284271,18 13.5,18.6715729 13.5,19.5 L13.5,21 C13.5,21.8284271 12.8284271,22.5 12,22.5 C11.1715729,22.5 10.5,21.8284271 10.5,21 L10.5,19.5 C10.5,18.6715729 11.1715729,18 12,18 Z M4.81066017,4.81066017 C5.39644661,4.22487373 6.34619408,4.22487373 6.93198052,4.81066017 L7.99264069,5.87132034 C8.57842712,6.45710678 8.57842712,7.40685425 7.99264069,7.99264069 C7.40685425,8.57842712 6.45710678,8.57842712 5.87132034,7.99264069 L4.81066017,6.93198052 C4.22487373,6.34619408 4.22487373,5.39644661 4.81066017,4.81066017 Z M4.81066017,19.2426407 C4.22487373,18.6568542 4.22487373,17.7071068 4.81066017,17.1213203 L5.87132034,16.0606602 C6.45710678,15.4748737 7.40685425,15.4748737 7.99264069,16.0606602 C8.57842712,16.6464466 8.57842712,17.5961941 7.99264069,18.1819805 L6.93198052,19.2426407 C6.34619408,19.8284271 5.39644661,19.8284271 4.81066017,19.2426407 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.pending_transfer_items') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Items Transfer   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','item-transfer') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-transfer" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.item_transatioms') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-item-transfer') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu item-transfer" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('item-transations.index')}}" class="menu-link menu-item-active" aria-haspopup="true">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.transfer_item') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-item-received') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu item-receive" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('item-transations.receive')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.receive_item') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Sales Kits   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','item-Offers') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-sales-kits" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.sales_kits') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-offers') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu sales-kits" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales-kit.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.sales_kits') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-add-offer') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-sales-kit" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales-kit.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_sales_kits') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-add-item-discount') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-item-discount" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('item-discount.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_item_discount') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Purchasing Invoices   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','purchase-invoice') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-purchase" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.purchasing_invoices') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-purchase-invoice') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu purchase-invoices" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('purchase_invoices.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.purchasing_invoices') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-add-purchase-invoice') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-purchase-invoices" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('purchase_invoices.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_purchasing_invoice') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-return-item-to-supplier') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-return_items" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('return_items.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.return_items') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Return Items   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','return-item-to-supplier') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-return" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.return_items') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','stock-return-item-to-supplier') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-return_items" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('return_items.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.return_items') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--   Options   --}}
{{--                                <li class="menu-item menu-item-submenu menu-item-rel menu-options" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                        <span class="menu-text">{{ __('topbar.subheader.options') }}</span>--}}
{{--                                        <span class="menu-desc"></span>--}}
{{--                                        <i class="menu-arrow"></i>--}}
{{--                                    </a>--}}
{{--                                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                        <ul class="menu-subnav">--}}
{{--                                            <li class="menu-item menu-item-submenu options" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                <a href="{{route('options.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                    <span class="menu-text">{{ __('topbar.subheader.company_options') }}</span>--}}
{{--                                                    --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}

{{--                                            <li class="menu-item menu-item-submenu add-options" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                <a href="{{route('options.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                    <span class="menu-text">{{ __('topbar.subheader.add_options') }}</span>--}}
{{--                                                    --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}

{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_4">--}}


{{--                        <div id="kt_header_menu_4" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   ACCOUNTING   --}}

{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-management') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-acc" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.accountsManagement') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-accounting') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item acc" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('accounts.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.accounts') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}

{{--                                                    <li class="menu-item acc-tree" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('accounts.tree')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.accounts_tree') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}

{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-add-accounting') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-acc" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('accounts.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('forms.accounts.add_account') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}


{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-accounting-transaction') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-trans" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.tansactions') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-view-transaction') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item trans" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('transactions.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.viewTransactions') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-add-transaction') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-trans" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('transactions.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_transaction') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-transfer-between-accounts') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('transactions.transfer')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.transfer') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-customers-groups') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-cst-pay" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.customer_payment') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}


{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-received-from-customer') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-cst-pay" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('createCustomerPayment')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_customer_payment') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}


{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-suppliers-groups') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-sup-pay" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.supplier_payment') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','accounting-pay-for-supplier') || $userHasGroup == 0)--}}

{{--                                                    <li class="menu-item menu-item-submenu add-sup-pay" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('createSupplierPayment')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_supplier_payment') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}


{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}


{{--                    --}}{{--Manufacturing--}}
{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_5">--}}


{{--                        <div id="kt_header_menu_5" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   CRM   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-suppliers') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-clients" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.Clients') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','suppliers') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('clients')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.Clients') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-add-suppliers') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('clients.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_client') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','suppliers-groups') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-clientsGroups" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="{{route('clients_groups')}}" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.ClientsGroups') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-suppliers-groups') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('clients_groups')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.ClientsGroups') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-add-suppliers-groups') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('create_clients_group')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_group') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--                                <li class="menu-item menu-item-submenu menu-item-rel menu-item-crm_tickets_tree" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                --}}{{--                                    <a href="{{route('clients_groups')}}" class="menu-link menu-toggle">--}}
{{--                                --}}{{--                                        <span class="menu-text">Crm Tickets Tree</span>--}}
{{--                                --}}{{--                                        <span class="menu-desc"></span>--}}
{{--                                --}}{{--                                        <i class="menu-arrow"></i>--}}
{{--                                --}}{{--                                    </a>--}}
{{--                                --}}{{--                                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                --}}{{--                                        <ul class="menu-subnav">--}}
{{--                                --}}{{--                                            <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                --}}{{--                                                <a href="{{route('createTicketTree')}}" class="menu-link">--}}
{{--                                --}}{{--																		<span class="svg-icon menu-icon">--}}
{{--                                --}}{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                --}}{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                --}}{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                --}}{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--                                --}}{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--                                --}}{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--                                --}}{{--																				</g>--}}
{{--                                --}}{{--																			</svg>--}}
{{--                                --}}{{--                                                                            <!--end::Svg Icon-->--}}
{{--                                --}}{{--																		</span>--}}
{{--                                --}}{{--                                                    <span class="menu-text">{{ __('forms.inventory.category.add') }}</span>--}}
{{--                                --}}{{--                                                    --}}{{----}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                --}}{{--                                                </a>--}}
{{--                                --}}{{--                                            </li>--}}


{{--                                --}}{{--                                            <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                --}}{{--                                                <a href="{{route('ticketsTree')}}" class="menu-link">--}}
{{--                                --}}{{--																		<span class="svg-icon menu-icon">--}}
{{--                                --}}{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                --}}{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                --}}{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                --}}{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                --}}{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--                                --}}{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--                                --}}{{--																				</g>--}}
{{--                                --}}{{--																			</svg>--}}
{{--                                --}}{{--                                                                            <!--end::Svg Icon-->--}}
{{--                                --}}{{--																		</span>--}}
{{--                                --}}{{--                                                    <span class="menu-text">Tickets Tree</span>--}}
{{--                                --}}{{--                                                    --}}{{----}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                --}}{{--                                                </a>--}}
{{--                                --}}{{--                                            </li>--}}

{{--                                --}}{{--                                        </ul>--}}
{{--                                --}}{{--                                    </div>--}}
{{--                                --}}{{--                                </li>--}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','tickets') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-item-tickets" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="{{route('show_tickets')}}" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('common.tickets') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-tickets') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('createTicket')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('forms.inventory.category.add') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-add-tickets') || $userHasGroup == 0)--}}

{{--                                                    <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('show_tickets')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('common.tickets') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>--}}
{{--                                                                                    <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                </g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.ticket_settings') }}</span>--}}
{{--                                                        <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                    <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                        <ul class="menu-subnav">--}}
{{--                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','categories') || $userHasGroup == 0)--}}
{{--                                                                <li class="menu-item menu-item-submenu menu-item-rel menu-item-crm_tickets_categories" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                                                    <a href="{{route('clients_groups')}}" class="menu-link menu-toggle">--}}
{{--                                                                        <span class="menu-text">{{ __('common.ticket_categories') }}</span>--}}
{{--                                                                        <span class="menu-desc"></span>--}}
{{--                                                                        <i class="menu-arrow"></i>--}}
{{--                                                                    </a>--}}
{{--                                                                    <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                                        <ul class="menu-subnav">--}}
{{--                                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-categories') || $userHasGroup == 0)--}}
{{--                                                                                <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                                                    <a href="{{route('CrmCategories')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                        <span class="menu-text">{{ __('common.ticket_categories') }}</span>--}}
{{--                                                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                            @endif--}}
{{--                                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-add-categories') || $userHasGroup == 0)--}}
{{--                                                                                <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                                                    <a href="{{route('createCrmCategory')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                        <span class="menu-text">{{ __('topbar.subheader.add_categories') }}</span>--}}
{{--                                                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                            @endif--}}

{{--                                                                        </ul>--}}
{{--                                                                    </div>--}}
{{--                                                                </li>--}}
{{--                                                            @endif--}}
{{--                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','status') || $userHasGroup == 0)--}}
{{--                                                                <li class="menu-item menu-item-submenu menu-item-rel menu-item-crm_tickets_status" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                                                    <a href="{{route('clients_groups')}}" class="menu-link menu-toggle">--}}
{{--                                                                        <span class="menu-text">{{ __('common.tickets_status') }}</span>--}}
{{--                                                                        <span class="menu-desc"></span>--}}
{{--                                                                        <i class="menu-arrow"></i>--}}
{{--                                                                    </a>--}}
{{--                                                                    <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                                        <ul class="menu-subnav">--}}
{{--                                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-status') || $userHasGroup == 0)--}}
{{--                                                                                <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                                                    <a href="{{route('createCrmTicketsStatus')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                        <span class="menu-text">{{ __('forms.form_header.add_status') }}</span>--}}
{{--                                                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                            @endif--}}
{{--                                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-add-status') || $userHasGroup == 0)--}}

{{--                                                                                <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                                                    <a href="{{route('CrmTicketsStatus')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                        <span class="menu-text">{{ __('common.tickets_status') }}</span>--}}
{{--                                                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                            @endif--}}
{{--                                                                        </ul>--}}
{{--                                                                    </div>--}}
{{--                                                                </li>--}}
{{--                                                            @endif--}}
{{--                                                            --}}{{--   Bill Of Materials   --}}
{{--                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','status-transfer-logic') || $userHasGroup == 0)--}}
{{--                                                                <li class="menu-item menu-item-submenu menu-item-rel menu-item-crm_transfer-logic" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                                                    <a href="{{route('clients_groups')}}" class="menu-link menu-toggle">--}}
{{--                                                                        <span class="menu-text">{{ __('common.tickets_transfer_logic') }}</span>--}}
{{--                                                                        <span class="menu-desc"></span>--}}
{{--                                                                        <i class="menu-arrow"></i>--}}
{{--                                                                    </a>--}}
{{--                                                                    <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                                        <ul class="menu-subnav">--}}
{{--                                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-status-transfer-logic') || $userHasGroup == 0)--}}
{{--                                                                                <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                                                    <a href="{{route('createTransferLogic')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                        <span class="menu-text">{{ __('forms.inventory.category.add') }}</span>--}}
{{--                                                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                            @endif--}}
{{--                                                                            @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','crm-add-status-transfer-logic') || $userHasGroup == 0)--}}
{{--                                                                                <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                                                    <a href="{{route('transferLogic')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                        <span class="menu-text">{{ __('common.tickets_transfer_logic') }}</span>--}}
{{--                                                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                                    </a>--}}
{{--                                                                                </li>--}}
{{--                                                                            @endif--}}
{{--                                                                        </ul>--}}
{{--                                                                    </div>--}}
{{--                                                                </li>--}}
{{--                                                            @endif--}}

{{--                                                            <li class="menu-item menu-item-submenu menu-item-rel crm-groups" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                                                <a href="{{route('clients_groups')}}" class="menu-link menu-toggle">--}}
{{--                                                                    <span class="menu-text">{{ __('topbar.subheader.crm_groups') }}</span>--}}
{{--                                                                    <span class="menu-desc"></span>--}}
{{--                                                                    <i class="menu-arrow"></i>--}}
{{--                                                                </a>--}}
{{--                                                                <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                                    <ul class="menu-subnav">--}}
{{--                                                                        <li class="menu-item cat" aria-haspopup="true">--}}
{{--                                                                            <a href="{{route('crm_groups.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                <span class="menu-text">{{ __('topbar.subheader.crm_groups') }}</span>--}}
{{--                                                                                --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                            </a>--}}
{{--                                                                        </li>--}}

{{--                                                                        <li class="menu-item menu-item-submenu add-cat" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                                            <a href="{{route('crm_groups.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                <span class="menu-text">{{ __('topbar.subheader.add_crm_groups') }}</span>--}}
{{--                                                                                --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                            </a>--}}
{{--                                                                        </li>--}}

{{--                                                                    </ul>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}

{{--                                                            <li class="menu-item menu-item-submenu menu-item-rel crm-status-groups" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                                                <a href="{{route('crm_status_groups.index')}}" class="menu-link menu-toggle">--}}
{{--                                                                    <span class="menu-text">{{ __('topbar.subheader.crm_status_groups') }}</span>--}}
{{--                                                                    <span class="menu-desc"></span>--}}
{{--                                                                    <i class="menu-arrow"></i>--}}
{{--                                                                </a>--}}
{{--                                                                <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                                    <ul class="menu-subnav">--}}
{{--                                                                        <li class="menu-item status-groups" aria-haspopup="true">--}}
{{--                                                                            <a href="{{route('crm_status_groups.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                <span class="menu-text">{{ __('topbar.subheader.crm_status_groups') }}</span>--}}
{{--                                                                                --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                            </a>--}}
{{--                                                                        </li>--}}

{{--                                                                        <li class="menu-item menu-item-submenu add-status-groups" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                                            <a href="{{route('crm_status_groups.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                <span class="menu-text">{{ __('topbar.subheader.add_crm_status_groups') }}</span>--}}
{{--                                                                                --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                            </a>--}}
{{--                                                                        </li>--}}

{{--                                                                    </ul>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}

{{--                                                            <li class="menu-item menu-item-submenu menu-item-rel crm-group-user" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                                                <a href="{{route('crm_group_users.index')}}" class="menu-link menu-toggle">--}}
{{--                                                                    <span class="menu-text">{{ __('topbar.subheader.crm_group_users') }}</span>--}}
{{--                                                                    <span class="menu-desc"></span>--}}
{{--                                                                    <i class="menu-arrow"></i>--}}
{{--                                                                </a>--}}
{{--                                                                <div class="menu-submenu menu-submenu-classic menu-submenu-right">--}}
{{--                                                                    <ul class="menu-subnav">--}}
{{--                                                                        <li class="menu-item user-group" aria-haspopup="true">--}}
{{--                                                                            <a href="{{route('crm_group_users.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<rect x="0" y="0" width="24" height="24" />--}}
{{--																					<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />--}}
{{--																					<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                <span class="menu-text">{{ __('topbar.subheader.crm_group_users') }}</span>--}}
{{--                                                                                --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                            </a>--}}
{{--                                                                        </li>--}}

{{--                                                                        <li class="menu-item menu-item-submenu add-status-groups" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                                            <a href="{{route('crm_group_users.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--																					<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																					<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																				</g>--}}
{{--																			</svg>--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                                                <span class="menu-text">{{ __('topbar.subheader.add_crm_group_users') }}</span>--}}
{{--                                                                                --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                                            </a>--}}
{{--                                                                        </li>--}}

{{--                                                                    </ul>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_6">--}}


{{--                        <div id="kt_header_menu_6" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   Bill Of Materials   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','manufacturing-boms') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-bom" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.boms') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','boms') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu bom" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('boms.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.boms') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}


{{--                                                    <li class="menu-item menu-item-submenu bom-availStock" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('bom.availStock')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.quantity_aganist_stock') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-boms') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-bom" datasales-kit-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('boms.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_bom') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--   MAN Cost Centers   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','cost-center') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-man-cc" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.cost_centers') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','manufacturing-cost-center') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu cost-centers" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('cost_centers.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.cost_centers') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-cost-center') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-cost_center" datasales-kit-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('cost_centers.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_cost_center') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--   MAN Work Orders   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','work-order') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-man-WO" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.work_orders') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','manufacturing-work-order') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu work-orders" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('work_orders.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.work_orders') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-work-order') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-work-order" datasales-kit-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('work_orders.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																					<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--																				</g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_work_order') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_7">--}}


{{--                        <div id="kt_header_menu_7" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   Sales Invoices   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-invoice') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-sales" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.sales_invoices') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','view-sales-invoice') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu sales-invoices" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales_invoices.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.sales_invoices') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-sales-invoice') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-sales-invoices" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales_invoices.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_sales_invoice') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-sales-invoice-POS') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-sales-invoices-pos" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales_invoices.createPOS')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_sales_invoice_pos') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{--  Return Sales Invoices   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-return-item-from-customers') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-customer_return" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.customer_returns') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','return-item-from-customers') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu customer_return" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('customer_item_returns.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.customer_returns') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-return-item-from-customer') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-customer_return" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('customer_item_returns.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_customer_returns') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}


{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{--  Sales Qoutation   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-quota') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-sales_quot" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.sales_quotation') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','view-sales-quota') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu sales_quot" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales_quotations.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.sales_quotation') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','add-sales-quota') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu add-sales_quot" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales_quotations.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.add_sales_quotation') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}


{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}


{{--                                --}}{{--   Stock Withdraw Request   --}}
{{--                                <li class="menu-item menu-item-submenu menu-item-rel menu-stock-withdraw" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                        <span class="menu-text">{{ __('topbar.subheader.stock_withdraw_request') }}</span>--}}
{{--                                        <span class="menu-desc"></span>--}}
{{--                                        <i class="menu-arrow"></i>--}}
{{--                                    </a>--}}
{{--                                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                        <ul class="menu-subnav">--}}
{{--                                            <li class="menu-item menu-item-submenu stock-withdraw-requests" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                <a href="{{route('stock_withdraw_requests.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                    <span class="menu-text">{{ __('topbar.subheader.stock_withdraw_requests') }}</span>--}}
{{--                                                    --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_8">--}}


{{--                        <div id="kt_header_menu_8" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   Sales Rep   --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-rep') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-sales-reports" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.sales_rep_report') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-rep-transaction-report') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu sales-rep" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales_rep.index')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.sales_rep_report_by_date') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales-rep-report') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu sales-rep-index" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('salesRepIndex')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.sales_rep_report') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{-- Items Reports --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','items') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-items-reports" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.items_report') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','out-of-stock-items-reports') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu out_of_stock" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('items.out_of_stock')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.out_of_stock') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','available-items-reports') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu available_items" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('items.available_items')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.available_items') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','item-card-reports') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu item_card" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('items.item_card')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.item_card') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                --}}{{-- Sales Reports --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-sales-reports" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.sales_report') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','most-sales-times-reports') || $userHasGroup == 0)--}}
{{--                                                    <li class="menu-item menu-item-submenu out_of_stock" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                        <a href="{{route('sales.most_sales_time')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                            <span class="menu-text">{{ __('topbar.subheader.most_sales_time') }}</span>--}}
{{--                                                            --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endif--}}

{{--                                                <li class="menu-item menu-item-submenu branch-sales" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('sales.branch')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.branch_sales') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}

{{--                                                <li class="menu-item menu-item-submenu branch-sales" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('sales.most_sales_items')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.most_sales_items') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="menu-item menu-item-submenu branch-sales" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('sales.most_profit_items')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.most_profit_items') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}

{{--                                                <li class="menu-item menu-item-submenu branch-sales" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('sales.most_sales_items_by_cat')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.most_sales_items_by_cat') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}

{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                --}}{{-- Customer & Supplier Transactions Reports --}}
{{--                                @if(\App\Helpers\CustomHelpers::getCollectionCount($collection,'slug','sales') || $userHasGroup == 0)--}}
{{--                                    <li class="menu-item menu-item-submenu menu-item-rel menu-sales-reports" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                        <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                            <span class="menu-text">{{ __('topbar.subheader.users') }}</span>--}}
{{--                                            <span class="menu-desc"></span>--}}
{{--                                            <i class="menu-arrow"></i>--}}
{{--                                        </a>--}}
{{--                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                            <ul class="menu-subnav">--}}
{{--                                                <li class="menu-item menu-item-submenu customer-transaction" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                    <a href="{{route('users.actions')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                        <span class="menu-text">{{ __('topbar.subheader.users_actions') }}</span>--}}
{{--                                                        --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}




{{--                    --}}{{--HR--}}
{{--                    <div class="tab-pane p-5 p-lg-0" id="kt_header_tab_9">--}}


{{--                        <div id="kt_header_menu_9" class="header-menu header-menu-mobile header-menu-layout-default kt_header_menu">--}}
{{--                            <!--begin::Nav-->--}}
{{--                            <ul class="menu-nav">--}}
{{--                                --}}{{--   Departments   --}}
{{--                                <li class="menu-item menu-item-submenu menu-item-rel menu-hr-departments" data-menu-toggle="click" aria-haspopup="true">--}}
{{--                                    <a href="javascript:;" class="menu-link menu-toggle">--}}
{{--                                        <span class="menu-text">{{ __('topbar.subheader.departments') }}</span>--}}
{{--                                        <span class="menu-desc"></span>--}}
{{--                                        <i class="menu-arrow"></i>--}}
{{--                                    </a>--}}
{{--                                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">--}}
{{--                                        <ul class="menu-subnav">--}}
{{--                                            <li class="menu-item menu-item-submenu departments" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                <a href="{{route('department-list')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                    <span class="menu-text">{{ __('topbar.subheader.departments_settings') }}</span>--}}
{{--                                                    --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                            <li class="menu-item menu-item-submenu add-departments" data-menu-toggle="hover" aria-haspopup="true">--}}
{{--                                                <a href="{{route('add-department.create')}}" class="menu-link">--}}
{{--																		<span class="svg-icon menu-icon">--}}
{{--																			<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->--}}
{{--                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>--}}
{{--                                                                                    <path d="M10,14 L5,14 C4.33333333,13.8856181 4,13.5522847 4,13 C4,12.4477153 4.33333333,12.1143819 5,12 L12,12 L12,19 C12,19.6666667 11.6666667,20 11,20 C10.3333333,20 10,19.6666667 10,19 L10,14 Z M15,9 L20,9 C20.6666667,9.11438192 21,9.44771525 21,10 C21,10.5522847 20.6666667,10.8856181 20,11 L13,11 L13,4 C13,3.33333333 13.3333333,3 14,3 C14.6666667,3 15,3.33333333 15,4 L15,9 Z" fill="#000000" fill-rule="nonzero"/>--}}
{{--                                                                                    <path d="M3.87867966,18.7071068 L6.70710678,15.8786797 C7.09763107,15.4881554 7.73079605,15.4881554 8.12132034,15.8786797 C8.51184464,16.2692039 8.51184464,16.9023689 8.12132034,17.2928932 L5.29289322,20.1213203 C4.90236893,20.5118446 4.26920395,20.5118446 3.87867966,20.1213203 C3.48815536,19.7307961 3.48815536,19.0976311 3.87867966,18.7071068 Z M16.8786797,5.70710678 L19.7071068,2.87867966 C20.0976311,2.48815536 20.7307961,2.48815536 21.1213203,2.87867966 C21.5118446,3.26920395 21.5118446,3.90236893 21.1213203,4.29289322 L18.2928932,7.12132034 C17.9023689,7.51184464 17.2692039,7.51184464 16.8786797,7.12132034 C16.4881554,6.73079605 16.4881554,6.09763107 16.8786797,5.70710678 Z" fill="#000000" opacity="0.3"/>--}}
{{--                                                                                </g>--}}
{{--                                                                            </svg><!--end::Svg Icon-->--}}
{{--                                                                            <!--end::Svg Icon-->--}}
{{--																		</span>--}}
{{--                                                    <span class="menu-text">{{ __('topbar.subheader.add_departments') }}</span>--}}
{{--                                                    --}}{{--                                                    <i class="menu-arrow"></i>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <!--end::Nav-->--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <!--begin::Tab Pane-->--}}
{{--                </div>--}}
{{--                <!--end::Tab Content-->--}}
{{--            </div>--}}
            <!--end::Header Menu Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Bottom-->
</div>
<!--end::Header-->

