<?php
namespace App\Observers;

use Auth;
use App\Models\UserAction;


class UserActionsObserver
{

    public function saved($model)
    {
        if ($model->wasRecentlyCreated == true) {
            // Data was just created
            $action = 'created';
        } else {
            // Data was updated
            $action = 'updated';
        }
        if (Auth::check()) {
            $data = json_encode($model->getDirty());

            UserAction::create([
                'user_id'      => Auth::user()->id,
                'action'       => $action,
                'action_model' => $model->getTable(),
                'action_id'    => $model->id,
                'data'         => $data,
            ]);
        }
    }

}
