<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        Invoice::observe(new \App\Observers\UserActionsObserver);
    }
    public function invoice_items_total()
    {
        return $this->hasMany(Invoice_item::class)
            ->select('*','item_id', \DB::raw('ROUND(sale_price, 2) as roundedUnitPrice'), \DB::raw('sum(quantity) as totalQuantity'))
            ->groupBy('item_id', 'roundedUnitPrice', 'discount_percentage');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by','id');
    }

    public function invoice_items(){
        return $this->hasMany(Invoice_item::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function branch(){
        return $this->belongsTo(Branch::class);
    }
}
