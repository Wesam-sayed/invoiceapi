<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice_item extends Model
{
    protected $table = 'invoice_items';

    public function item(){
        return $this->belongsTo(Item::class);
    }

    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
}
