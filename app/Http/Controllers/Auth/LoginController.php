<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Company;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
            if (auth()->user()->active == 1) {
                if (auth()->user()->company_id != null){

                    $company = Company::find(auth()->user()->company_id);
                    $branch = Branch::find(auth()->user()->branch_id);
                    $userCompany['id']= $company->id;
                    $userCompany['name']= $company->name;
                    session(['company' => $userCompany]);
                    session(['branch' => $branch->id ?? null]);
                    session(['branch_name' => Auth()->user()->branch->name ?? null]);
                    session(['email' => Auth()->user()->email ?? null]);
                    session(['image_url' => Auth()->user()->image_url ?? null]);

                }
                return redirect()->route('invoice.index');
            }else{
                return redirect()->route('login');
            }
        }else{
            return redirect()->route('login')
                ->with(['message'=>'Email-Address And Password Are Wrong.','message-type'=> "error"]);
        }

    }

    protected function logout(Request $request)
    {
        \Auth::logout();
        return redirect()->route('login');
    }
}
