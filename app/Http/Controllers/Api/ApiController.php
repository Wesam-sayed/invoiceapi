<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\createInvoiceItems;
use App\Models\Invoice;
use App\Models\Invoice_item;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Spatie\ArrayToXml\ArrayToXml;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function createInvoice(Request $request)
    {
        $res_arr = array();
        $error = $this->validations($request, "createInvoice");
        if ($error['errors']) {
            return $this->prepareResult(false, [], $error['errors'], "Error in Entered Data", '400');
        }else{
            $job = new createInvoiceItems($request->all());
            $result = dispatch($job);
            if (!$job->handle()){
                return $this->prepareResult(false, [], "Error While Save Data", "Error While Save Data", '400');
            }else{
                return $this->prepareResult(true,['success']);
            }

        }

    }

    public function getInvoice(Request $request)
    {
        $res_arr = array();
        $error = $this->validations($request, "getInvoice");
        if ($error['errors']) {
            return $this->prepareResult(false, [], $error['errors'], "Error in Entered Data", '400');
        }else{
            $invoice = Invoice::find($request->invoiceId);

            if (is_null($invoice)){
                return $this->prepareResult(false, [], "Please Enter The Correct Invoice Id", "Error in Entered Data", '400');
            }
            $res_arr['invoice']['_attributes'] = array('id'=>$invoice->id);
            $res_arr['invoice']['customer'] = 'Customer 1';
            $res_arr['invoice']['total_amount'] = number_format($invoice->total_amount,2);
            $res_arr['invoice']['amount_without_tax'] = number_format($invoice->amount_without_tax,2);
            $res_arr['invoice']['tax_amount'] = number_format($invoice->tax_amount,2);
            $res_arr['invoice']['discount'] = number_format($invoice->discount,2);
            $res_arr['invoice']['company'] = $invoice->company->name ;
            $res_arr['invoice']['branch'] = $invoice->branch->name;
            $res_arr['invoice']['note'] = $invoice->note ?? "";
            $res_arr['invoice']['created_by'] = $invoice->createdBy->name;
            $res_arr['invoice']['created_at'] = Carbon::parse($invoice->created_at)->format('Y-m-d');


            foreach ($invoice->invoice_items as $key => $record){
                $res_arr['items'][$key]['_attributes'] = array('id'=>$record->item_id);
                $res_arr['items'][$key]['id'] = $record->item_id;
                $res_arr['items'][$key]['name'] = $record->item_name;
                $res_arr['items'][$key]['quantity'] = number_format($record->quantity);
                $res_arr['items'][$key]['sale_price'] = number_format($record->sale_price);
                $res_arr['items'][$key]['note'] = number_format($record->sale_price);
                $res_arr['items'][$key]['created_at'] = Carbon::parse($record->created_at)->format('Y-m-d');
            }


            if ($request->has('type') && $request->type == 1){
                $arrayToXml = new ArrayToXml($res_arr);
                $result = $arrayToXml->prettify()->toXml();

                $headers = ['Content-Type' => 'application/xml'];
                $fileName = 'invoice-'.$invoice->id.'.xml';
                $xmlFilePath = public_path('invoices/'.$fileName);

                if (!file_exists($xmlFilePath)){
                    file_put_contents($xmlFilePath, $result);
                }
                return response()->download($xmlFilePath);
            }

//            $response = response()->make($result, 200, [   'Content-Type' => 'text/xml']);
//            $response->header('Cache-Control', 'public');
//            $response->header('Content-Description', 'File Transfer');
//            $response->header('Content-Disposition', 'attachment; filename=test.xml');
//            $response->header('Content-Transfer-Encoding', 'binary');
//            $response->header('Content-Type', 'text/xml');
//            return $this->prepareResult(true,$res_arr);


           return $this->prepareResult(true,$res_arr);

        }



    }

    public function validations($request, $type) {
        $errors = [];
        $error = false;
        //dd($request->patientID);
        if ($type == "createInvoice") {
            $validator = \Validator::make($request->all(), [
                'user_id' => 'required|int',
                'company_id' => 'required|int',
                'customer_id' => 'required|int',
                'branch_id' => 'required|int',
                'items.*.id' => 'required|int',
                'items.*.quantity' => 'required|numeric|between:0,999999999999999',
                'items.*.unit_price' => 'required|numeric|between:0,999999999999999',
            ]);
            if ($validator->fails()) {
                $error = true;
                $errors = $validator->errors();

            }
        }
        if ($type == "getInvoice") {
            $validator = \Validator::make($request->all(), [
                'invoiceId' => 'required|int',
            ]);
            if ($validator->fails()) {
                $error = true;
                $errors = $validator->errors();

            }
        }
        return array("error" => $error, "errors" => $errors);
    }

    private function prepareResult($status, $data, $errors = '', $msg = '', $code = 200) {
        if ($status == true) {
            $return = array('status'=>true,'code'=>$code,'data' => $data);
        } else {
            if (is_string($errors)) {
                $message = $errors;
            } else {
                $message = $errors->messages();
            }
            $return = array('status'=>false,'code'=>$code,'data' => $data, 'message_en' => $message, 'message_ar' => $message, 'errors' => '');
        }
        return response()->json($return, $code);
    }
}
