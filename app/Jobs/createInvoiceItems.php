<?php

namespace App\Jobs;

use App\Models\Invoice;
use App\Models\Invoice_item;
use App\Models\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class createInvoiceItems implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    protected $response = true;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {


            // create Invoice
            $invoiceFields = [
                'invoice_price_type'=>1,
                'customer_id'=>$this->data['customer_id'],
                'company_id'=>$this->data['company_id'],
                'branch_id'=>$this->data['branch_id'],
                'note'=>$this->data['note'] ?? '',
                'created_by'=>\Auth::user()->id ?? null,
            ];

            $invoiceId = Invoice::create($invoiceFields);
            $invoiceId = $invoiceId->id;
            $total_discount = 0;
            $total_taxes = 0;
            $amount_without_tax = 0;
            $netAmount = 0;
            foreach ($this->data['items'] as $item){

                $item_details = Item::find($item['id']);

                // create invoice items

                $invoice_item = new Invoice_item();
                $invoice_item->invoice_id = $invoiceId;
                $invoice_item->item_id = $item['id'];
                $invoice_item->item_name = $item_details->name;
                $invoice_item->branch_id = $this->data['branch_id'];
                $invoice_item->quantity = $item['quantity'];
                $invoice_item->sale_price = $item['unit_price'];
                $invoice_item->unit_price = $item_details->cost_price;
                $invoice_item->discount_percentage = $item_details->discount_percentage;
                $invoice_item->tax_percentage = $item_details->tax_percentage;
                $invoice_item->withholding_tax_percentage = $item_details->withholding_tax_percentag ?? 0;
                $invoice_item->excise_tax_percentage = $item_details->excise_tax_percentage ?? 0;
                $invoice_item->purchase_tax_percentage = $item_details->purchase_tax_percentage ?? 0;
                $invoice_item->created_by = \Auth::user()->id ?? null;
                $invoice_item->updated_by = \Auth::user()->id ?? null;
                $invoice_item->save();

                // calc items

                $itemAmount = $item['quantity'] * $item['unit_price'];
                $total_discount += ($item_details->discount_percentage / 100) * ($itemAmount);
                $tax = ($item_details->tax_percentage / 100) * ($itemAmount);
                $withholding_tax = ($item_details->withholding_tax_percentag / 100) * ($itemAmount);
                $excise_tax = ($item_details->excise_tax_percentage / 100) * ($itemAmount);
                $purchase_tax = ($item_details->purchase_tax_percentage / 100) * ($itemAmount);
                $total_taxes += ($tax + $withholding_tax + $excise_tax + $purchase_tax);
                $amount_without_tax +=  ($itemAmount) - ($total_discount);
                $netAmount +=  ( $itemAmount + $total_taxes ) - $total_discount;


            }

            // update invoice with calc total items amount
            $oldInvoice = Invoice::find($invoiceId)->update([
                'amount_without_tax'=>$amount_without_tax,
                'tax_amount'=>$total_taxes,
                'discount'=>$total_discount,
                'total_amount'=>$netAmount,
            ]);


            \DB::commit();
            $this->response = true;

        } catch (\Exception $e) {
            \DB::rollback();
            $this->response = false;
        }

        return $this->response;
    }

}
