<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::post('login', [\App\Http\Controllers\Api\ApiController::class,'authenticate']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', [\App\Http\Controllers\Api\ApiController::class,'getAuthenticatedUser']);
    Route::post('/invoice/create', [\App\Http\Controllers\Api\ApiController::class,'createInvoice']);
    Route::get('/invoice', [\App\Http\Controllers\Api\ApiController::class,'getInvoice']);
});
