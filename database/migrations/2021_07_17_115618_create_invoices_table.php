<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('customer_id');
            $table->integer('branch_id');
            $table->boolean('invoice_price_type')->default(0)->comment('0 -> retail , 1 -> wholesale');
            $table->decimal('amount_without_tax', 20, 5)->default(0);
            $table->decimal('tax_amount', 20, 5)->default(0);
            $table->decimal('discount', 20, 5)->default(0);
            $table->decimal('total_amount', 20, 5)->default(0);
            $table->string('note');

            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();

            $table->index('branch_id');
            $table->index('company_id');
            $table->index('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
