<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_id');
            $table->integer('item_id');
            $table->string('item_name');
            $table->integer('branch_id');
            $table->decimal('quantity', 20, 5)->default(0);
            $table->decimal('sale_price', 20, 5)->default(0);
            $table->decimal('unit_price', 20, 5)->default(0);
            $table->decimal('discount_percentage', 20, 5)->default(0);
            $table->decimal('tax_percentage', 20, 5)->default(0);
            $table->decimal('withholding_tax_percentage', 20, 5)->default(0);
            $table->decimal('excise_tax_percentage', 20, 5)->default(0);
            $table->decimal('purchase_tax_percentage', 20, 5)->default(0);
            $table->string('note')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');

            $table->timestamps();

            $table->index('invoice_id');
            $table->index('item_id');
            $table->index('branch_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
