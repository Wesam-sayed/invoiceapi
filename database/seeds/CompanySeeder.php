<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('companies')->truncate();

        $dataArr = array(
            [
                'name'=>'Company 1',
                'mobile'=>'01061101235',
                'email'=>'company@gmail.com',
                'active'=>1,
            ],
        );

        foreach ($dataArr as $record){
            \App\Models\Company::create($record);
        }
    }
}
