<?php

use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('items')->truncate();

        $dataArr = array(
            [
                'name'=>'Item-1',
                'company_id'=>1,
                'serial_no'=>'0001',
                'sku'=>'prod-01',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'tax_percentage'=>14,
                'withholding_tax_percentage'=>10,
                'excise_tax_percentage'=>3,
                'purchase_tax_percentage'=>5,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'Item الاول',
                'active'=>1,
            ],
            [
                'name'=>'Item-2',
                'company_id'=>1,
                'serial_no'=>'0002',
                'sku'=>'prod-02',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'tax_percentage'=>14,
                'withholding_tax_percentage'=>10,
                'excise_tax_percentage'=>3,
                'purchase_tax_percentage'=>5,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'Item الثاني',
                'active'=>1,
            ],
            [
                'name'=>'Item-3',
                'company_id'=>1,
                'serial_no'=>'0003',
                'sku'=>'prod-03',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'tax_percentage'=>14,
                'withholding_tax_percentage'=>10,
                'excise_tax_percentage'=>3,
                'purchase_tax_percentage'=>5,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'Item الثالث',
                'active'=>1,
            ],
            [
                'name'=>'Item-4',
                'company_id'=>1,
                'serial_no'=>'0004',
                'sku'=>'prod-04',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'tax_percentage'=>14,
                'withholding_tax_percentage'=>10,
                'excise_tax_percentage'=>3,
                'purchase_tax_percentage'=>5,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'Item الرابع',
                'active'=>1,
            ],
            [
                'name'=>'Item-5',
                'company_id'=>1,
                'serial_no'=>'0005',
                'sku'=>'prod-05',
                'cost_price'=>20,
                'retail_price'=>100,
                'wholesale_price'=>60,
                'tax_percentage'=>14,
                'withholding_tax_percentage'=>10,
                'excise_tax_percentage'=>3,
                'purchase_tax_percentage'=>5,
                'discount_percentage'=>2,
                'quantity'=>1,
                'image_url'=>'/uploads/items/default-1',
                'reorder_level'=>10,
                'description'=>'Item الخامس',
                'active'=>1,
            ],
        );

        foreach ($dataArr as $record){
            \App\Models\Item::create($record);
        }
    }
}
