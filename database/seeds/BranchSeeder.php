<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('branches')->truncate();

        $dataArr = array(
            [
                'name'=>'Branch 1',
                'code'=>'br-1',
                'desc'=>'Branch for Company 1',
                'company_id'=>1,
                'managed_by'=>1,
                'active'=>1,
                'mobile'=>'01061101235',
                'email'=>'company@gmail.com',
            ],
        );

        foreach ($dataArr as $record){
            \App\Models\Branch::create($record);
        }
    }
}
