<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();

        $userArr = array(
            [
                'company_id'=>1,
                'branch_id'=>1,
                'name'=>'Admin',
                'email'=>'Admin@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('123456'),
                'active'=>1,
            ],
        );

        foreach ($userArr as $record){
            \App\User::create($record);
        }
    }
}
